-- AdwaitaAppearance.hs

module Var.Theme.AdwaitaAppearance where

-- imports
import XMonad

import XMonad.Layout.Spacing

myFont :: String
myFont = "xft:RobotoMono Nerd Font:regular:size=10:antialias=true:hinting=true"
myFontB :: String
myFontB = "xft:RobotoMono Nerd Font:regular:size=32:antialias=true:hinting=true"

myBorderWidth :: Dimension
myBorderWidth = 3

myWBorder :: Border
myWBorder = (Border 0 3 3 0)
mySBorder :: Border
mySBorder = (Border 3 0 0 3)

-- color
myFGC :: String
myFGC = "#2E2E2E"

myGreyC :: String
myGreyC = "#DEDAD7"

myLightGreyC :: String
myLightGreyC = "#F6F5F4"

myBGC :: String
myBGC = myLightGreyC

myAccentC :: String
myAccentC = "#3584E4"

myLightAccentC :: String
myLightAccentC = "#54B2FF"

myDarkAccentC :: String
myDarkAccentC = "#3584E4"

myAccentC2 :: String
myAccentC2 = "#006356"

myLightAccentC2 :: String
myLightAccentC2 = "#09B099"

myUrgC :: String
myUrgC = "#f34a10"
