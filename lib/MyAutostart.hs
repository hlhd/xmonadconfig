-- MyAutostart.hs

module MyAutostart where

-- Import
import XMonad
import XMonad.Util.SpawnOnce
import XMonad.Hooks.SetWMName

import MyVariables

myStartupHook :: X ()
myStartupHook = do
    spawnOnce "setxkbmap -option 'ctrl:swapcaps'"
    spawnOnce "~/.local/mybin/fix-xfce4-panel-disappear.sh"
    spawnOnce "~/.local/mybin/toggle-compton.sh"
    spawnOnce "~/.local/mybin/start-redshift.sh"
    spawnOnce "~/.local/mybin/touchpad-set.sh"
    setWMName "LG3D"
